USE [Test] 
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Class](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClassName] [nvarchar](50) NULL,
	[Location] [nvarchar](100) NOT NULL,
	[TeacherName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Class] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Class] ON
INSERT [dbo].[Class] ([Id], [ClassName], [Location], [TeacherName]) VALUES (1, N'UI/UX', N'Building 5 Room 501', N'Mr Johnston')
INSERT [dbo].[Class] ([Id], [ClassName], [Location], [TeacherName]) VALUES (2, N'DevOps', N'Building 2 Room 606', N'Miss Thomson')
INSERT [dbo].[Class] ([Id], [ClassName], [Location], [TeacherName]) VALUES (11, N'.NET C#', N'Building 4 Room 606', N'Mr Green')
SET IDENTITY_INSERT [dbo].[Class] OFF
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentName] [nvarchar](50) NOT NULL,
	[Age] [int] NULL,
	[GPA] [decimal](18, 1) NULL,
	[ClassId] [int] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Student] ON
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (1, N'John Packer', 18, CAST(3.2 AS Decimal(18, 1)), 1)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (6, N'Annie Vu', 23, CAST(3.6 AS Decimal(18, 1)), 2)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (13, N'Ted Baker', 33, CAST(3.3 AS Decimal(18, 1)), 11)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (18, N'Peter Johnston', 19, CAST(2.5 AS Decimal(18, 1)), 1)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (19, N'Robert Smith', 20, CAST(3.1 AS Decimal(18, 1)), 1)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (20, N'Louise Thomson', 21, CAST(2.1 AS Decimal(18, 1)), 1)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (21, N'Vince Henry', 22, CAST(3.2 AS Decimal(18, 1)), 11)
INSERT [dbo].[Student] ([Id], [StudentName], [Age], [GPA], [ClassId]) VALUES (22, N'Umang Patel ', 33, CAST(3.0 AS Decimal(18, 1)), 11)
SET IDENTITY_INSERT [dbo].[Student] OFF
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_Student_Class] FOREIGN KEY([ClassId])
REFERENCES [dbo].[Class] ([Id])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_Student_Class]
GO
