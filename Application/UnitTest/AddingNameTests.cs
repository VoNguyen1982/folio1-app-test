﻿using System;
using Application.Contexts;
using Application.Models;
using Application.Processes;
using Application.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Data.Entity;
using System.Data.Entity.Migrations;


namespace UnitTest
{
    [TestClass]
    public class AddingNameTests
    {
        TestDbContext context = new TestDbContext();
        StudentProcess process = null;

        public AddingNameTests()
        {
            Database.SetInitializer(new TestInitializer());
            context.Database.Initialize(true);
            process = new StudentProcess(new StudentService(context), context);
        }

        [TestMethod]
        public void AddStudentSuccessfully()
        {
            
            var a = process.AddOrUpdate(new Student() {StudentName = "Student Black", Id = 2, ClassId = 1});
            Assert.AreEqual("Student has been added.",a);

        }

        [TestMethod]
        public void AddStudentSameSurnameFailed1()
        {
            context.Students.AddOrUpdate(new Student() { StudentName = "Student Black", Id = 2, ClassId = 1 });
            context.SaveChanges();

            StudentProcess process = new StudentProcess(new StudentService(context), context);
            var a = process.AddOrUpdate(new Student() { StudentName = "Student01 Black", Id = 3, ClassId = 2 });

            Assert.AreEqual("surname cannot be added twice.", a);
        }

        [TestMethod]
        public void AddStudentSameSurnameFailed2()
        {
            context.Students.AddOrUpdate(new Student() { StudentName = "Black", Id = 2, ClassId = 1 });
            context.SaveChanges();

            StudentProcess process = new StudentProcess(new StudentService(context), context);
            var a = process.AddOrUpdate(new Student() { StudentName = "Student01 Black", Id = 3, ClassId = 2 });

            Assert.AreEqual("surname cannot be added twice.", a);
        }
    }
}
