﻿using System.Collections.Generic;
using System.Linq;
using Application.Contexts;
using Application.Models;
using Application.Services;

namespace Application.Processes
{
    public class StudentProcess
    {
        private IStudentService service = null;
        private IDbContext context = null;

        public StudentProcess(IStudentService _service, IDbContext _context)
        {
            context = _context;
            service = _service;
        }

        public string DeteleById(int id)
        {
            var aStudent = context.Set<Student>().FirstOrDefault(a => a.Id == id);
            if (aStudent != null)
            {
                if (service.DeleteStudent(aStudent))
                {
                    return "Student has been deleted";
                }

            }
            return "Cannot delete this student";
        }

        public string AddOrUpdate(Student aStudent)
        {

            if (aStudent != null)
            {
                string name = GetSurname(aStudent.StudentName);

                var surnameStudents = context.Set<Student>().Where(a => a.StudentName.ToLower().Contains(name) && a.Id != aStudent.Id);
                bool isDuplicated = false;
                foreach (var student in surnameStudents)
                {
                    if (CheckName(student.StudentName, name))
                    {
                        isDuplicated = true;
                        break;
                    }
                }

                if ((!isDuplicated && CheckName(aStudent.StudentName, name)) || (isDuplicated && !CheckName(aStudent.StudentName, name)) || (!isDuplicated && !CheckName(aStudent.StudentName, name)))
                {
                    if (service.AddOrUpdateStudent(aStudent))
                        return "Student has been added.";
                    else return "Cannot add or update this student.";
                }
                else return "surname cannot be added twice.";
            }
            else return "Cannot add or update this student.";
        }

        public bool CheckName(string name, string name1)
        {
            return (name.Trim().LastIndexOf(' ') >= 0 &&
                   name.Trim()
                       .Substring(name.Trim().LastIndexOf(' ') + 1).ToLower().Equals(name1)) || name.Trim().ToLower().Equals(name1);
        }

        public string GetSurname(string name)
        {
            if (name.Trim().LastIndexOf(' ') >= 0)
                return name.Trim()
                    .Substring(name.Trim().LastIndexOf(' ') + 1).ToLower();
            else return name.Trim().ToLower();
        }


        public IEnumerable<Student> GetStudentsByClassId(int id)
        {
            return service.GetStudentsByClassId(id);
        }

        public Student GetStudentsByStudentId(int id)
        {
            return service.GetStudentsByStudentId(id);
        }

    }
}