﻿using System.Collections.Generic;
using System.Linq;
using Application.Contexts;
using Application.Models;
using Application.Services;

namespace Application.Processes
{
    public class ClassProcess
    {
        private IClassService service = null;
        private IDbContext context = null;

        public ClassProcess(IClassService _service, IDbContext _context)
        {
            context = _context;
            service = _service;
        }

        public string DeteleById(int id)
        {
            var aClass = context.Set<Class>().FirstOrDefault(a => a.Id == id);
            if (aClass != null && !aClass.Students.Any())
            {
                if (service.DeleteClass(aClass))
                {
                    return "Class has been deleted";
                }
            }
            return "Cannot delete this class";
        }

        public string AddOrUpdate(Class aClass)
        {
            if (aClass != null)
            {
                if (service.AddOrUpdateClass(aClass))
                {
                    return "Class has been added/updated";
                }

            }
            return "Cannot add/update this class";
        }

        public IEnumerable<Class> GetAllClasses()
        {
            return service.GetAllClasses();
        }

        public Class GetAllClassById(int id)
        {
            return service.GetAllClassById(id);
        }

    }
}