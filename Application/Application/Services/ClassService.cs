﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Application.Contexts;
using Application.Models;

namespace Application.Services
{
    public interface IClassService
    {
        IEnumerable<Class> GetAllClasses();
        Class GetAllClassById(int id);
        bool AddOrUpdateClass(Class aClass);
        bool DeleteClass(Class aClass);
    }

    public class ClassService : IClassService
    {
        IDbContext _context = null;

        public ClassService(IDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Class> GetAllClasses()
        {
            try
            {
                return _context.Set<Class>().AsEnumerable();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Class GetAllClassById(int id)
        {
            try
            {
                return _context.Set<Class>().FirstOrDefault(a => a.Id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool AddOrUpdateClass(Class aClass)
        {
            try
            {
                _context.Set<Class>().AddOrUpdate(aClass);
                _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteClass(Class aClass)
        {
            try
            {
                _context.Set<Class>().Remove(aClass);
                _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}