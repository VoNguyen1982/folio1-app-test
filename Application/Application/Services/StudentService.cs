﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Application.Contexts;
using Application.Models;

namespace Application.Services
{
    public interface IStudentService
    {
        IEnumerable<Student> GetStudentsByClassId(int classId);
        bool AddOrUpdateStudent(Student aStudent);
        bool DeleteStudent(Student aStudent);
        Student GetStudentsByStudentId(int id);
    }

    public class StudentService : IStudentService
    {
        IDbContext _context = null;

        public StudentService(IDbContext context)
        {
            _context = context;
        }

        public IEnumerable<Student> GetStudentsByClassId(int classId)
        {
            try
            {
                return _context.Set<Student>().Where(a => a.ClassId == classId);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Student GetStudentsByStudentId(int id)
        {
            try
            {
                return _context.Set<Student>().FirstOrDefault(a => a.Id == id);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public bool AddOrUpdateStudent(Student aStudent)
        {
            try
            {
                _context.Set<Student>().AddOrUpdate(aStudent);
                _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool DeleteStudent(Student aStudent)
        {
            try
            {
                _context.Set<Student>().Remove(aStudent);
                _context.SaveChangesAsync();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}