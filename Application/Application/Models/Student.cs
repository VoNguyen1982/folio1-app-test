using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Application.Models
{
    [Table("Student")]
    public partial class Student
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string StudentName { get; set; }

        public int? Age { get; set; }

        public decimal? GPA { get; set; }

        public int ClassId { get; set; }

        public virtual Class Class { get; set; }
    }
}
