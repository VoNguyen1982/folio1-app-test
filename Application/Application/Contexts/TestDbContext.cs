﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using Application.Models;

namespace Application.Contexts
{
    public partial class TestDbContext : DbContext, IDbContext
    {
        public TestDbContext()
            : base()
        {
        }
        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<Student> Students { get; set; }
    }

    public class TestInitializer : DropCreateDatabaseAlways<TestDbContext>
    {
        protected override void Seed(TestDbContext context)
        {
            context.Classes.AddOrUpdate(new Class()
            {
                Id = 1,
                ClassName = "Class01",
                Location = "Location01",
                TeacherName = "Teacher01",
                Students = new List<Student>()
            {new Student(){Id=1,Age=1,ClassId = 1,GPA=1,StudentName = "Student01"}}});
            base.Seed(context);
        }
    }
}