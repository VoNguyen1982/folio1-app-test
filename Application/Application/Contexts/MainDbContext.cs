using System.Threading.Tasks;
using Application.Models;
using System;
using System.Data.Entity;

namespace Application.Contexts
{
    public interface IDbContext : IDisposable
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;

        Task<int> SaveChangesAsync();
    }


    public partial class MainDbContext : DbContext, IDbContext
    {
        public MainDbContext()
            : base("name=MainDbContext")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public virtual DbSet<Class> Classes { get; set; }
        public virtual DbSet<Student> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Class>()
                .HasMany(e => e.Students)
                .WithRequired(e => e.Class)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Student>()
                .Property(e => e.GPA)
                .HasPrecision(18, 1);
        }
    }
}