﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Application.Contexts;
using Application.Models;
using Application.Processes;
using Application.Services;
using Newtonsoft.Json;

namespace Application.Controllers
{
    public class StudentController : Controller
    {
        private IDbContext _dbContext;
        private StudentProcess _studentProcess;

        public StudentController()
        {
            _dbContext = new MainDbContext();
            _studentProcess = new StudentProcess(new StudentService(_dbContext), _dbContext);
        }

        public string GetStudentByClassId(int classId)
        {
            var students = _studentProcess.GetStudentsByClassId(classId);
            return Newtonsoft.Json.JsonConvert.SerializeObject(students, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
        }

        public ActionResult Add(int? classId = 0)
        {
            if (classId > 0)
            {
                var aStudent = new Student();
                aStudent.ClassId = classId.Value;
                return View(aStudent);
            }
            else
            {
                TempData["Message"] = "Please select class before adding student.";
                return RedirectToAction("Index", "Class");
            }
        }

        [HttpPost]
        public ActionResult Add(Student model)
        {
            if (ModelState.IsValid)
            {
                TempData["Message"] = _studentProcess.AddOrUpdate(model);
                return RedirectToAction("Index", "Class");
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var aStudent = _studentProcess.GetStudentsByStudentId(id);
            return View(aStudent);
        }

        [HttpPost]
        public ActionResult Edit(Student model)
        {
            if (ModelState.IsValid)
            {
                TempData["Message"] = _studentProcess.AddOrUpdate(model);
                return RedirectToAction("Index", "Class");
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            TempData["Message"] = _studentProcess.DeteleById(id);
            return RedirectToAction("Index", "Class");
        }
    }
}