﻿using System.Web.Mvc;
using Application.Contexts;
using Application.Models;
using Application.Processes;
using Application.Services;

namespace Application.Controllers
{
    public class ClassController : Controller
    {

        private IDbContext _dbContext;
        private ClassProcess _classProcess;

        public ClassController()
        {
            _dbContext = new MainDbContext();
            _classProcess = new ClassProcess(new ClassService(_dbContext),_dbContext);
        }

        public ActionResult Index()
        {
            var model = _classProcess.GetAllClasses();
            return View(model);
        }

        public ActionResult Add()
        {
            var aClass = new Class();
            return View(aClass);
        }

        [HttpPost]
        public ActionResult Add(Class model)
        {
            if (ModelState.IsValid)
            {
                TempData["Message"] = _classProcess.AddOrUpdate(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            var aClass = _classProcess.GetAllClassById(id);
            return View(aClass);
        }

        [HttpPost]
        public ActionResult Edit(Class model)
        {
            if (ModelState.IsValid)
            {
                TempData["Message"] = _classProcess.AddOrUpdate(model);
                return RedirectToAction("Index");
            }
            else
            {
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            TempData["Message"] = _classProcess.DeteleById(id);
            return RedirectToAction("Index");
        }

    }
}
