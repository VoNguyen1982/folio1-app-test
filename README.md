Start test at 10.00 Sunday.


Step to run this project

1.	Create a database in MSSQL and run the script.sql against this database. You will need to change the name of database in 1st line of the script.
(The MSSQL compact does not support VS2012 so I am afraid that we need to run sqlscript)

2.	Modify the name and credential of connection string in the web.config

3.	I was using VS2012 with MVC5 + MSSQL 2008R2 


Notes

1.	In order to make this test in good practice, I turn the lazy load option on in database context. Therefore, the index page will be loaded faster because the class objects will be populated without students. Once users click into a class record then the AJAX call will retrieve students belonging to this class and show in table.

2.	I made a mistake to put student name (title, surname) in one column therefore it makes thing difficult to verify the surname is unique. However, I think it is good to show my skill while working string so I don�t refactor the table again.

3.  In order to show how I test the logic, I only created 3 test cases in unit test.


Looking forward to seeing the feedback.

Thanks
Vo Nguyen
